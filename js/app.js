
// Wut?
if ( ! apps) var apps = {};
if ( ! apps.waldo) apps.waldo = {};

if ( ! Waldo) Waldo = {};
if ( ! Waldo.View) Waldo.View = {};

// What is *path? 
Goald.Application = Backbone.Router.extend({

	setup: function() {
		Backbone.history.start({
			pushState: true,
		});
	},

	routes: {
		'': "home",
		'test': "test",
		'*path': "notfound"
	},

	home: function() {
		console.debug("Started the home route");
	},

	test: function() {
		console.debug("Test Route");
	},

	notfound: function() {
		console.debug("404");
	},
});

apps.solidgoald = new Goald.Application();
apps.solidgoald.setup();
