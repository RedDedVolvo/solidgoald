this["Goald"] = this["Goald"] || {};
this["Goald"]["Templates"] = this["Goald"]["Templates"] || {};

this["Goald"]["Templates"]["folders"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape;with (obj) {__p += '<h3>' +((__t = ( name )) == null ? '' : __t) +'</h3>\n';}return __p};

this["Goald"]["Templates"]["idea"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape;with (obj) {__p += '<img src="img/text-document.svg">\n<h3>' +((__t = ( title )) == null ? '' : __t) +'</h3>\n<p>' +((__t = ( content )) == null ? '' : __t) +'</p>\n<p>' +((__t = ( created_at )) == null ? '' : __t) +'</p>';}return __p};