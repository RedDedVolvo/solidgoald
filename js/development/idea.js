var app = {};


app.IdeaModel = Backbone.Model.extend({
	localStorage: true,
	defaults: {
		title: ' ',
		content: ' ',
		created_at: ''
	}

});

app.IdeaCollection = Backbone.Collection.extend({
	model: app.IdeaModel,
	localStorage: chrome.storage.sync,
	addIdea: function(elements, options){
		return this.add(elements, options);
	},
	// This is doing nothing right now, but it is supposed to keep the collection persistent through different sessions.
	syncCollection: function(){
		Backbone.sync('create', this);
	}

});

var ideaCollection = new app.IdeaCollection();

app.defaultView = Backbone.View.extend({
	el: '#idea-list',

	initialize: function() {
		this.collection = ideaCollection;
		ideaCollection.fetch();
	},

	render: function() {
		var template = Goald["Templates"]["idea"];
		this.collection.each(_.bind(function(model){
			var content = template(model.toJSON());
			this.$el.append('<div class="idea-nugget">' + renderedContent + '</div>');
		}, this));
	}
});

var defaultview = new app.defaultView();
defaultview;
console.debug(ideaCollection);

app.CreateIdeaView = Backbone.View.extend({
	el: '#inputForm',
	// Don't need tagname because this view is hooked into inputform
	// tagName: 'div',
	// model: app.IdeaModel,

	initialize: function() {
		// Initialize model and collection
		this.collection = ideaCollection;

		this.listenTo(this.collection, "change", this.render);
		console.log("Create Idea Initialize Complete");
	},

		render: function() {
		var create_idea = new app.CreateIdeaView();
	},

	// This will need to be changed later so that there can be more buttons.

	events: {
		'submit form': 'updateModel',
		// 'submit form': 'clearForm'
	},

	updateModel: function(e){
		e.preventDefault();
		var newIdea = { title: $('#idea-name').val(), content: $('#idea-content').val(), created_at: Date()};
		this.collection.addIdea(new app.IdeaModel(newIdea));
		this.collection.syncCollection();
		chrome.storage.sync.set(ideaCollection);
		form.reset();
		console.log(this.collection);
	},

});

var create_idea = new app.CreateIdeaView;
create_idea;
console.debug("Create Idea Run")

app.IdeaListView = Backbone.View.extend({
	el: '#idea-list',
	// Don't need tagname because this view is hooked into inputform
	// tagName: 'div',
	// model: app.IdeaModel,

	initialize: function() {
		// Initialize model and collection
		this.collection = ideaCollection;
		this.listenTo(this.collection, "add", this.render);
		console.log("Idea List View Initialize Complete");
	},

	render: function() {
		var template = Goald["Templates"]["idea"];
		this.$el.html('');
		this.collection.each(_.bind(function(model){
			var renderedContent = template(model.toJSON());
			this.$el.append('<div class="idea-nugget">' + renderedContent + '</div>'); // Could append be why it's not loading on refresh?
			console.log(model);
		}, this));
	}

});

var idealist = new app.IdeaListView();
idealist.render();
console.debug("Idea List View Run");