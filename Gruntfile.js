module.exports = function(grunt) {
	grunt.initConfig({
		watch: {
			css: {
				files: ['style/*.less'],
				tasks: ['less']
			},
			js: {
				files: ['js/development/idea.js'],
				tasks: ['uglify']
			},
			templates: {
				files: ['templates/*.jst'],
				tasks: ['jst']
			}
		},

		uglify: {
			app: {
				files: {
					'js/production/minified.js': ['js/development/**/*.js']
				}
			}
		},

		less: {
			development: {
				options: {
					paths: ["/"],
				},
				files: {
					"style/style.css": "style/app.less"
				}
			},
		},

		// underscore: {
		// 	options: {
		// 		namespace: 'folders.html'
		// 	},
		// 	'templates.js': ['folders.html']
		// },

		jst: {
			compile: {
				options: {
					namespace: "Goald.Templates",
					processName: function (path) {
						var name = path.replace('templates/', '');
						    name = name.replace('.jst', '');

						return name.replace('/', '.'); 
					},

					templateSettings: {
					
					},
					prettify: true
				},
				files: {
					'js/production/templates.js': ['templates/*.jst']
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-underscore-compiler');
	grunt.loadNpmTasks('grunt-contrib-jst');

	grunt.registerTask('default', ['less', 'jst', 'uglify']);

}
